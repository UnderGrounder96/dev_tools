#!/usr/bin/env bash

FLAG="${1}"

set -euo pipefail

source configs/common

function vagrant_destroy(){
    _logger_info "WARNING: Destroying Virtual Machine"
    vagrant destroy -f
}

# trap vagrant_destroy EXIT

function vagrant_install(){
    _logger_info "Installing Vagrant"
    VAGRANT_VERSION=`curl -Nks https://releases.hashicorp.com/vagrant/ | grep -Eom1 '_[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}'`
    VAGRANT_VERSION=${VAGRANT_VERSION/_/}
    sudo dnf install -y https://releases.hashicorp.com/vagrant/${VAGRANT_VERSION}/vagrant_${VAGRANT_VERSION}_x86_64.rpm

    # Optional
    _logger_info "Installing Vagrant's optional plugins"
    vagrant plugin install vagrant-hostmanager vagrant-scp
}

function virtualbox_install(){
    _logger_info "Installing Virtualbox required packages"
    sudo dnf install -y binutils kernel-devel kernel-headers libgomp make patch gcc glibc-headers glibc-devel dkms

    _logger_info "Please restart your system"
    # reboot

    _logger_info "Installing Virtualbox"
    sudo dnf config-manager --add-repo=https://download.virtualbox.org/virtualbox/rpm/el/virtualbox.repo
    sudo rpm --import https://www.virtualbox.org/download/oracle_vbox.asc
    sudo dnf install -y VirtualBox-6.0
    systemctl status vboxdrv
    sudo usermod ${USER} -aG vboxusers
    # sudo /sbin/vboxconfig

    # Optional
    # _logger_info "Installing Virtualbox's optional extpack"
    # wget http://download.virtualbox.org/virtualbox/6.0.24/Oracle_VM_VirtualBox_Extension_Pack-6.0.24.vbox-extpack
    # VBoxManage extpack install Oracle_VM_VirtualBox_Extension_Pack-6.0.24.vbox-extpack
}


function main(){
    # vagrant_install
    # virtualbox_install

    vagrant_destroy

    [ -f Vagrantfile ] || {
      _logger_info "There is no Vagrantfile"; exit 1
    }

    _logger_info "Starting the vagrant machine"
    vagrant up

    _logger_info "Running build.sh"
    vagrant ssh -c "sudo bash /vagrant/build/build.sh ${FLAG}"

    # _logger_info "Aquiring build.log"
    # vagrant scp ":/vagrant/logs/build*.log" logs/

    # _logger_info "Executing rsync-auto"
    # vagrant rsync-auto
}

main
