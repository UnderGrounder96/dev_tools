#!/usr/bin/env bash

declare -A STRINGS

# han solo
STRINGS[product]="dev_tools"
STRINGS[title]="${STRINGS[product]} installer!"
STRINGS[unexpected_error]="Unexpected error occurred!"
STRINGS[welcome_msg]="Installer will guide you through the installation process.\n\n
If any problem occurs during installation, please open an isse at https://gitlab.com/UnderGrounder96/dev_tools/-/issues"

# dialog
STRINGS[dialog_backtitle]="${STRINGS[product]} – Short for “development tools”"
STRINGS[dialog_msgbox_success]="The installation procedure finished!"
STRINGS[dialog_modules_msg]="Please choose the ${STRINGS[product]} modules.\n\n\
Use the UP/DOWN arrow keys to navigate and SPACE for toggling the option."

# setup
STRINGS[setup_title]="${STRINGS[product]} | Setup"
STRINGS[setup_modules]="${STRINGS[product]} | Choose modules"
STRINGS[setup_progress]="${STRINGS[product]} | Installation progress"
STRINGS[setup_conf]="${STRINGS[product]} | Configuration"

# installation
STRINGS[installation]="${STRINGS[product]} (and selected modules)"
STRINGS[installation_prod_abort]="An error occurred during ${STRINGS[product]} installation. Exiting"
STRINGS[installation_logs]="${STRINGS[product]} installation logs can be found at $ROOT_DIR/logs/"
STRINGS[installation_success]="${STRINGS[installation]} installation concluded successfully\n\n${STRINGS[installation_logs]}"
STRINGS[installation_interrupted]="${STRINGS[installation]} installation has been interrupted\n\n${STRINGS[installation_logs]}"
STRINGS[installation_aborted]="${STRINGS[installation]} installation aborted\n\n${STRINGS[installation_logs]}"
STRINGS[installation_prod_started]="${STRINGS[product]} installation started\n\n${STRINGS[installation_logs]}"

# unused
STRINGS[installation_confirm]="Continue the ${STRINGS[installation]} installation?\n\n
The following tools will be installed:\n\n"
STRINGS[installation_abort_confirm]="Abort the ${STRINGS[installation]} installation? [y/n] "
