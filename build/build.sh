#!/usr/bin/env bash

FLAG="$1"
ROOT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/.."
LOG_FILE="$ROOT_DIR/logs/build-$(date '+%F_%T').log"

export ROOT_DIR LOG_FILE

set -euo pipefail

source $ROOT_DIR/configs/common

function prepare_tmp_dir(){
    _logger_info "Preparing tmp dir"
    export TMP_DIR=$(mktemp -d)
    pushd "$TMP_DIR"
}

function clean_tmp_dir(){
    _logger_info "Cleaning tmp dir"
    popd
    # rm -rf "$TMP_DIR"
}


function gitea_install(){
    _logger_info "Installing Gitea"

    DL_GITEA_URL="https://dl.gitea.io/gitea"
    GITEA_VER=`wget -qO- ${DL_GITEA_URL} | grep -Po "(?<=Current Release ).*?(?=\")"`
    GITEA_LIN_URL="${DL_GITEA_URL}/${GITEA_VER}/gitea-${GITEA_VER}-linux-amd64"
    GITEA_SERVICE_URL="https://raw.githubusercontent.com/go-gitea/gitea/v${GITEA_VER}/contrib/systemd/gitea.service"

    useradd --system --shell /bin/bash --create-home --home /home/git \
      --comment 'Git Version Control' git

    mkdir -vp /etc/gitea /var/lib/gitea/{custom,data,indexers,public,log}
    chown git: /var/lib/gitea/{data,indexers,log}
    chmod 750 /var/lib/gitea/{data,indexers,log}
    chown root:git /etc/gitea; chmod 770 /etc/gitea

    wget --progress=bar:force ${GITEA_LIN_URL} -O /usr/local/bin/gitea
    chmod +x /usr/local/bin/gitea

    wget --progress=bar:force ${GITEA_SERVICE_URL} -P /etc/systemd/system/
    systemctl daemon-reload
    systemctl enable --now gitea

    _logger_info "Please configure gitea via http://localhost:3000"
    sleep 30s

    chmod 750 /etc/gitea
    chmod 640 /etc/gitea/app.ini
}

function jenkins_install(){
    _logger_info "Installing Jenkins"

    wget --progress=bar:force -P /etc/yum.repos.d https://pkg.jenkins.io/redhat-stable/jenkins.repo
    rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key

    dnf install -y java-11-openjdk-devel jenkins
    echo "jenkins    ALL = NOPASSWD : ALL" >> /etc/sudoers
    echo 'JAVA_HOME="/usr/lib/jvm/java-11-openjdk"' >> ~/.bashrc

    sudo sh -c "systemctl start jenkins && systemctl enable jenkins"

    _logger_info "Please visit http://localhost:8080/login?from=%2F"
    sleep 15s

    _logger_info "Your password is"
    cat /var/lib/jenkins/secrets/initialAdminPassword

    sleep 15s
}

function kubectl_install(){
    _logger_info "Installing Kubernetes"

    _logger_info "Installing docker, conntrack-tools"
    dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
    dnf install -y docker-ce conntrack-tools
    systemctl enable --now docker

    wget --progress=bar:force "https://dl.k8s.io/release/$(curl -sSL https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
    install kubectl /bin/kubectl
    kubectl completion bash > /etc/bash_completion.d/kubectl

    # echo 'export PATH="$PATH:$HOME/.local/bin:$HOME/bin:/usr/local/bin"' >> ~/.bash_profile
    # export PATH="$PATH:$HOME/.local/bin:$HOME/bin:/usr/local/bin"

    _logger_info "Installing k9s"
    curl -sS https://webinstall.dev/k9s | bash

    # Helm
    # curl -sS https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash

    wget --progress=bar:force https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
    install minikube-linux-amd64 /bin/minikube

    minikube start --driver=none --addons=ingress
    echo "minikube start --driver=none" >> ~/.bash_profile

    git clone https://github.com/UnderGrounder96/node-hello-world.git

    kubectl apply -f node-hello-world/config/
    kubectl rollout status deployment/node-hello-world

    kubectl get services -o wide
    kubectl get pods -o wide
}

function install_all() {
    _logger_info "Performing install_all"

    gitea_install

    jenkins_install

    # kubectl_install

    exit 0
}

function main(){
    prepare_tmp_dir

    _extra_installs

    [ "$FLAG" == "-a" ] && install_all

    export -f gitea_install jenkins_install #kubectl_install

    bash $ROOT_DIR/build/dialog.sh

    # _logger_info "Updating all packages"
    # dnf upgrade -y

    clean_tmp_dir
    exit 0
}

main 2>&1 | tee -a $LOG_FILE