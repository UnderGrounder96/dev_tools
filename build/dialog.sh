#!/usr/bin/env bash

set -euo pipefail

source $ROOT_DIR/configs/strings.sh

# Define the dialog exit status codes
: ${DIALOG_OK=0}
: ${DIALOG_ERROR=-1}
: ${DIALOG_CANCEL=1}
: ${DIALOG_HELP=2}
: ${DIALOG_EXTRA=3}
: ${DIALOG_ITEM_HELP=4}
: ${DIALOG_ESC=255}

trap trap_exit EXIT

function trap_exit() {
    EXIT_CODE="$?"
    sed -i '/.*\x1b.*/d' $LOG_FILE

    case "${EXIT_CODE}" in
      0)
        :
        ;;
      1|255)
        _logger_info "${STRINGS[installation_interrupted]}"
        exit ${EXIT_CODE}
        ;;
      *)
        _logger_info "${STRINGS[installation_prod_abort]}"
        exit ${EXIT_CODE}
        ;;
    esac
}

function invoke_diag() {
    sudo dnf install -y dialog

    MATRIX=(
        Gitea "Hosting software development version control" on
        Jenkins "Automation server" on
        # Kubernetes "Container-orchestration system" off
    )

    CHOICES=$(dialog --clear --stdout --separate-output --backtitle "${STRINGS[dialog_backtitle]}" \
        --title "${STRINGS[title]}" --checklist "${STRINGS[dialog_modules_msg]}" 0 0 0 "${MATRIX[@]}")

    _logger_info "${STRINGS[installation_prod_started]}"

    for module in $CHOICES ; do
        case $module in
            "Gitea")
                gitea_install
                ;;

            "Jenkins")
                jenkins_install
                ;;

            "Kubernetes")
                kubectl_install
                ;;
        esac
        _logger_info "${module} was installed successfully"
    done

    _logger_info "${STRINGS[installation_success]}"
}

invoke_diag
