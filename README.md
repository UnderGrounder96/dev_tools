# dev_tools

This small project installs development tools under a EL CentOS 8 Virtual Machine.

## Requirements

- [Vagrant](https://www.vagrantup.com/intro)
- [VirtualBox](https://virtualbox.org)
- `vagrant-hostmanager vagrant-scp` plugins (optional)
- **More than 2 CPU cores**
- **More than 4GB of RAM**
- **More than 10 GB Disk space**

## Deployment

The script is fool-proof to a degree. It should help you avoid the most basic errors.

To run the VM, simply execute the command:

```bash
vagrant plugin install vagrant-hostmanager vagrant-scp

bash vagrant_start.sh -a
```

To install locally: `sudo bash build/build.sh -a`

For a more interactive installation one could remove the `-a` from the script call.

## Using docker-compose

Alternatively to deploy in [Docker](https://docs.docker.com/get-docker/),
while using [docker-compose](https://docs.docker.com/compose/install/).

One could execute: `cp .env.default .env && docker-compose up -d`

## Tools

Gitea - [http://localhost:3000](http://localhost:3000)

Jenkins - [http://localhost:8080](http://localhost:8080)

Kubectl - [http://localhost:30000.32767](http://localhost:30000.32767)
